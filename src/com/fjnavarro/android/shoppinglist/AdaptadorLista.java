package com.fjnavarro.android.shoppinglist;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

class AdaptadorLista extends BaseAdapter {
	Context contexto;
	// Almacenamos una lista de instancias de "Lista"
	ArrayList<Lista> datos;
	
	AdaptadorLista(Context contexto, ArrayList<Lista> datos){
        this.contexto = contexto;
        this.datos = datos;
	}

	public int getCount() {
        return this.datos.size();
	}
	
	public Lista getItem(int position) {
	        return this.datos.get(position);
	}
	
	public long getItemId(int position) {
	        return position;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
	        Lista item = this.getItem(position);
	        // definimos el layout
	        View v = View.inflate(this.contexto, R.layout.row, null);
	        //obtenemos los controles de la vista
	        TextView lNombre = (TextView)v.findViewById(R.id.nombreLista);
	        // definimos las propiedas de los controles con los datos del item
	        lNombre.setText(item.getNombre());
	
	        return v;
	}
	
	public void deleteItem(int position){
		Lista item = this.getItem(position);
		
		this.datos.remove(position);
		
		// borramos una lista con todos sus items relacionados
		try{
			//Comprobamos el estado de la memoria externa (tarjeta SD)
			if(Utilities.getSD()==true){
	    		//si el SD est� disponible.
				
				// obtenemos el path externo de nuestra aplicaci�n para borrar las im�genes		
				File file = new File(contexto.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/"+String.valueOf(item.getId())+"/");
				
				if (file.isDirectory()) {
					// borramos las im�genes que est�n dentro del directorio
			        String[] children = file.list();
			        for (int i = 0; i < children.length; i++) {
			            new File(file, children[i]).delete();
			        }
			        
			        // borramos el directorio que conten�a las im�genes de la lista
			        boolean dir = new File(contexto.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/"+String.valueOf(item.getId())+"/").delete();
			    }
			}
		} catch (Exception e) {
			// Capturamos un error grave
			Log.e("Shopping List", "Error grave "+e.toString());
		}
	}
}