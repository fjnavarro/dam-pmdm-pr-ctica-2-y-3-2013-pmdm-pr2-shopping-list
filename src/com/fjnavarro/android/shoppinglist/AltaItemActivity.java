package com.fjnavarro.android.shoppinglist;

import java.io.File;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ImageButton;
import android.widget.ImageView;

public class AltaItemActivity extends Activity {

	int list_id;
	private BDDAssistan BDDAss;
	private SQLiteDatabase db;
	
	Uri imgTemporal = null;
	File file = null;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alta_item);

		try{
			//Comprobamos el estado de la memoria externa (tarjeta SD)
			if(Utilities.getSD()==true){
	    		//si el SD est� disponible.
				
				// obtenemos el path externo de nuestra aplicaci�n para almacenar im�genes		
			    // enviamos la uri para almacenar la imagen
				file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temporal.jpg");
				imgTemporal = Uri.fromFile(file);
			}
		} catch (Exception e) {
			// Capturamos un error grave
			Log.e("Shopping List", "Error grave "+e.toString());
		}
		//Recuperamos la informaci�n pasada en el intent
        Bundle bundle = this.getIntent().getExtras();
        
		this.list_id = bundle.getInt("id_lista");
		final String nombreLista = bundle.getString("nombreLista");

		// obtenemos una referencia a los controles del interface
		final EditText txtNombre = (EditText) findViewById(R.id.TxtNombre);
		final Button btnGuardar = (Button) findViewById(R.id.BtnGuardar);
		final Button btnCancelar = (Button) findViewById(R.id.BtnCancelar);
		// bot�n obtener imagen
		final ImageButton BtnObtImagen = (ImageButton) findViewById(R.id.BtnObtImagen);

		try{
			// Nos conectamos a la base de datos
			BDDAss = new BDDAssistan(this);
		}catch(SQLiteException e){
			// Capturamos un error grave con la base de datos
			Log.e("Shopping List", "Error grave con la BD "+e.toString());
		}catch(Exception e){
			// Capturamos un error grave y no es de la bd
			Log.e("Shopping List", "Error grave "+e.toString());
		}

		// Implementamos el evento onclick del bot�n guardar
		btnGuardar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String nombre = txtNombre.getText().toString();

				if (!nombre.equals("")) {
					try{
						// creamos una nueva lista en la bs
						db = BDDAss.getWritableDatabase();

						// Insertamos un registro en la bd como objeto ContentValues
						ContentValues nuevoRegistro = new ContentValues();
						nuevoRegistro.put("name", nombre);
						nuevoRegistro.put("list_id",list_id);
						nuevoRegistro.put("buyed",0);
						// insertamos el registro y obtenemos el id de la insercci�n
						long idItem = db.insert("element", null, nuevoRegistro);
						if(idItem!=-1){
							// si el id es correcto renombramos la imagen temporal a una imagen definitiva
							
							// comprobamos que exista la imagen temporal
							if(file.exists()){
								// si ya no existe creamos un directorio dentro del directorio de im�genes con el id de la lista
								File path = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), String.valueOf(list_id));
								path.mkdirs();
								      
								// renombramos la imagen
								File fileFinal = new File(path, String.valueOf(idItem) +".jpg");
								    
								file.renameTo(fileFinal);
							}
							
						}
					}catch(SQLiteException e){
						// Capturamos un error grave con la base de datos
						Log.e("Shopping List", "Error grave con la BD "+e.toString());
					}catch(Exception e){
						// Capturamos un error grave y no es de la bd
						Log.e("Shopping List", "Error grave "+e.toString());
					}finally{
		    			// liberamos recursos
		    			// cerarmos la conexi�n con la bd
		    			db.close();
		    		}
				}

				// volvemos al listado de los items.
				Intent intent = new Intent(AltaItemActivity.this, ListadoItemActivity.class);
				
				// devolvemos las propiedades de la lista al listado de items.
				Bundle b = new Bundle();
		        b.putInt("id_lista",list_id);
		        b.putString("nombreLista", nombreLista);
		        intent.putExtras(b);
				
				// Iniciamos la nueva actividad
				startActivity(intent);
				finish();
			}
		});

		// Implementamos el evento onclick del bot�n cancelar
		btnCancelar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// volvemos al listado de los items.
				Intent intent = new Intent(AltaItemActivity.this, ListadoItemActivity.class);
				
				// devolvemos las propiedades de la lista al listado de items.
				Bundle b = new Bundle();
		        b.putInt("id_lista",list_id);
		        b.putString("nombreLista", nombreLista);
		        intent.putExtras(b);
				
				// Iniciamos la nueva actividad
				startActivity(intent);
				finish();
			}
		});
		
		// Implementamos el evento onclick del bot�n obtener imagen
		BtnObtImagen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// damos al bot�n funcionabilidad como si fuera presionado (durante unos segundos) y lanzara un contextMenu
				registerForContextMenu(v);
			    openContextMenu(v);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_alta_item, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		// mostramos un men� contextual, se ha modificado su comportamiento habitual para usarlo al pulsar en el icono de la c�mara
		
		MenuInflater inflater = getMenuInflater();

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

		// definimos el menu a mostrar 
		inflater.inflate(R.menu.menu_ctx_alta_item, menu);

	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		// creamos las acciones al pulsar en una opci�n del men� contextual
		
		// definimos el intent necesario para comunicarnos con la c�mara y galer�a
		Intent intent;
				
	    switch (item.getItemId()) {
	        case R.id.CtxOpc2:
	        	try{
		        	// obtener la imágen desde la galería
		        	intent =  new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
			    	
			    	// iniciamos la actividad para que devuelva el resultado y podamos capturarlo con onActivityResult
			    	startActivityForResult(intent, 2);
	        	} catch (Exception e) {
	    			// Capturamos un error grave
	    			Log.e("Shopping List", "Error grave "+e.toString());
	    		}
	        	
	            return true;
	        case R.id.CtxOpc3:
	        	try{
		        	// obtener la imágen desde la cámara de fotos
		        	intent =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
		        	
		        	// enviamos un parámetro con la url de la imagen temporal
			    	intent.putExtra(MediaStore.EXTRA_OUTPUT, imgTemporal);
			    	
			    	// iniciamos la actividad para que devuelva el resultado y podamos capturarlo con onActivityResult
			    	startActivityForResult(intent, 3);
			    } catch (Exception e) {
					// Capturamos un error grave
					Log.e("Shopping List", "Error grave "+e.toString());
				}
	        	
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Este método se ejecuta cuando se ha iniciado una actividad con startActivityResult y devuelve un resultado
		try{
			if (requestCode == 2) {
				// La actividad viene desde la galer�a
	            if (resultCode == RESULT_OK) {
	            	// si todo ha ido bien
	            	
	            	//Comprobamos el estado de la memoria externa (tarjeta SD)
	    			if(Utilities.getSD()==true){
	            		// y el SD est� disponible.
	            		
		            	// obtenemos la uri de la im�gen seleccionada
		            	Uri selectedImage = data.getData();
		            	// formateamos la imagen y descargamos en el path del FILE
		            	Utilities.getImagenTransformada(selectedImage, file, AltaItemActivity.this);
		            	
		            	//  obtenemos una referencia al control de la imagen
		            	ImageView iv = (ImageView)findViewById(R.id.ImgFotoGaleria);
		            	// definimos la im�gen temporal a mostrar
		    			iv.setImageURI(selectedImage);
	            	}else{
	            		Log.e("Shopping List", "Error no tenemos acceso al SD.");
	            	}
	            }
			}else if(requestCode == 3){
				// La actividad viene desde la c�mara de fotos
				if (resultCode == RESULT_OK) {
	            	// si todo ha ido bien
					
					//Comprobamos el estado de la memoria externa (tarjeta SD)
					if(Utilities.getSD()==true){
	            		// y el SD est� disponible.
						
		            	// formateamos la imagen capturada y la sobreescribimos
		            	Utilities.getImagenTransformada(imgTemporal, file, AltaItemActivity.this);		            	
						//  obtenemos una referencia al control de la imagen
						ImageView iv = (ImageView)findViewById(R.id.ImgFotoGaleria);
						// definimos la im�gen temporal a mostrar
		    			iv.setImageURI(imgTemporal);
					}else{
	            		Log.e("Shopping List", "Error no tenemos acceso al SD.");
	            	}
				}
	        }
		} catch (Exception e) {
			// Capturamos un error grave
			Log.e("Shopping List", "Error grave "+e.toString());
		}
	}
}
