package com.fjnavarro.android.shoppinglist;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

class AdaptadorItem extends BaseAdapter {
	Context contexto;
	private BDDAssistan BDDAss;
	private SQLiteDatabase db;
	
	// Almacenamos una lista de instancias de "Item"
	ArrayList<Item> datos;
	
	AdaptadorItem(Context contexto, ArrayList<Item> datos){
        this.contexto = contexto;
        this.datos = datos;
	}

	public int getCount() {
        return this.datos.size();
	}
	
	public Item getItem(int position) {
	        return this.datos.get(position);
	}
	
	public long getItemId(int position) {
	        return position;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		final Item item = this.getItem(position);
				
		//Comprobamos el estado de la memoria externa (tarjeta SD)
		
		
		// definimos el layout
		View v = View.inflate(contexto, R.layout.row_item, null);
		//obtenemos los controles de la vista
		TextView lNombre = (TextView) v.findViewById(R.id.text1);
		//  obtenemos una referencia al control de la imagen
    	ImageView iv = (ImageView) v.findViewById(R.id.ImgFoto);
		
		// definimos las propiedas de los controles con los datos del item
		lNombre.setText(item.getNombre());
		
		// comprobamos que exista la imagen del item
		try{
			//Comprobamos el estado de la memoria externa (tarjeta SD)
			if(Utilities.getSD()==true){
	    		//si el SD est� disponible.
				
				// obtenemos el path externo de nuestra aplicaci�n para almacenar im�genes y el nombre del fichero que deber�a tener		
				File file = new File(contexto.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/"+String.valueOf(item.getListId())+"/"+item.getId()+".jpg");
				if(file.exists()){
					// si existe el fichero lo mostramos en el listado
					Uri img = Uri.fromFile(file);
					iv.setImageURI(img);
				}
			}
		} catch (Exception e) {
			// Capturamos un error grave
			Log.e("Shopping List", "Error grave "+e.toString());
		}
		
		if (item.getComprado()) {
			// ponemos la fila con tramsparencia para mostrar que est� comprado
			v.setAlpha((float) 0.5);
		}
		
		// si se ha pulsado en la fila del item	
		v.setOnClickListener(new OnClickListener() {
	         public void onClick(View v) {
	        	 try{
						// Nos conectamos a la base de datos
						BDDAss = new BDDAssistan(contexto);
						db = BDDAss.getWritableDatabase();
						
						// si no estaba comprado
						if (item.getComprado()==false) {
							// Actualizamos en la bd el estado del item, comprado
							db.execSQL("UPDATE element SET buyed=1 WHERE id="+item.getId());
							item.setComprado(true);
							notifyDataSetChanged();
						} else {
							// Actualizamos en la bd el estado del item, no comprado
							db.execSQL("UPDATE element SET buyed=0 WHERE id="+item.getId());
							item.setComprado(false);
							notifyDataSetChanged();
						}
					}catch(SQLiteException e){
						// Capturamos un error grave con la base de datos
						Log.e("Shopping List", "Error grave con la BD "+e.toString());
					}catch(Exception e){
						// Capturamos un error grave y no es de la bd
						Log.e("Shopping List", "Error grave "+e.toString());
					}finally{
		    			// liberamos recursos
		    			// cerarmos la conexi�n con la bd
		    			db.close();
		    		}
	         }
	      });
		
		return v;
	}
	
	public void deleteItem(int position){
		Item item = this.getItem(position);
		
		this.datos.remove(position);
		
		// borramos la im�gen del item
		try{
			//Comprobamos el estado de la memoria externa (tarjeta SD)
			if(Utilities.getSD()==true){
	    		//si el SD est� disponible.
				
				// obtenemos el path externo de nuestra aplicaci�n para almacenar im�genes y el nombre del fichero que deber�a tener		
				File file = new File(contexto.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/"+String.valueOf(item.getListId())+"/"+item.getId()+".jpg");
				if(file.exists()){
					// si existe el fichero lo borramos
					 file.delete();
				}
			}
		} catch (Exception e) {
			// Capturamos un error grave
			Log.e("Shopping List", "Error grave "+e.toString());
		}
	}
}