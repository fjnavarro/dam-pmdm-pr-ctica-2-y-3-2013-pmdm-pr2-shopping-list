package com.fjnavarro.android.shoppinglist;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class Utilities {
	/*
	 * Clase con utilidades auxiliares - metodos est�ticos
	 * */
	public static void getImagenTransformada(Uri selectedImage, File file, Context context){
		/*
		 * Obtiene una imagen desde un URI, la formatea, rescala y copia a un FILE
		 * */
		InputStream is;
		// el tama�o  en px que queremos obtener
		final float tamanio = 150;
		
		try {
			is = context.getContentResolver().openInputStream(selectedImage);
			
			// preparamos las opciones para formatear la im�gen resultante
			BitmapFactory.Options options = new BitmapFactory.Options();
			
			// obtenemos la imagen
	    	BufferedInputStream bis = new BufferedInputStream(is);
	    	Bitmap bitmap = BitmapFactory.decodeStream(bis,null,options);
	    	
	    	// obtenemos el tama�o de la im�gen
	    	int imageWidth = bitmap.getWidth();
	    	int imageHeight = bitmap.getHeight();
	    	
	    	// hacemos el calculo para obtener la escala
	        float scaleHeight = (float)imageHeight/(float)tamanio;
	        float scaleWidth  = (float)imageWidth /(float)tamanio;
	        float scale = 0;
	        
	        if (scaleWidth < scaleHeight){
	        	scale = scaleHeight;
	        }else{
	        	scale = scaleWidth;
	        }

	        // rescalamos la imagen y teniendo en cuenta la densidad de la pantalla
	        bitmap = Bitmap.createScaledBitmap(bitmap, (int)(imageWidth/scale), (int)(imageHeight/scale), true);  
	        			
	    	// comprimimos la im�gen resultante en un fichero
	    	FileOutputStream out = new FileOutputStream(file);
	    	bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);
		} catch (FileNotFoundException e) {
			Log.e("Shopping List", "Error: " + e.getMessage());
		}
	}
	
	public static boolean getSD(){
		/*
		 * Obtener el estado de la memoria externa (tarjeta SD)
		 * */		
		boolean sdDisponible = false;
		boolean sdAccesoEscritura = false;
		
		//Comprobamos el estado
		String estado = Environment.getExternalStorageState();		 
		if (estado.equals(Environment.MEDIA_MOUNTED)){
		    sdDisponible = true;
		    sdAccesoEscritura = true;
		} else if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY)){
			sdDisponible = true;
			sdAccesoEscritura = false;
		}
	
		if(sdAccesoEscritura==true&&sdDisponible==true){
			// est� disponible y tenemos permisos de escritura
			return true;
		}else{
			return false;
		}
	}
}