package com.fjnavarro.android.shoppinglist;
/*
 * Almacena registros de la tabla element
 * */
public class Item {
	private int id;
	private int list_id;
	private String nombre;
	private boolean comprado;
	
	public Item(int id, int list_id, String nombre, int comprado){
		this.id = id;
		this.nombre = nombre;
		this.list_id = list_id;
		// si el registro es 1(true) � 0(false) lo ponemos en la clase
		// como true(1) o false(0)
		if(comprado==1){
			this.comprado = true;
		}else{
			this.comprado = false;
		}
	}
	// getters
	public int getId(){
		return this.id;
	}
	public String getNombre(){
		return this.nombre;
	}
	public int getListId(){
		return this.list_id;
	}
	public boolean getComprado(){
		return this.comprado;
	}
	// setter
	public void setComprado(boolean value){
		this.comprado=value;
	}
}