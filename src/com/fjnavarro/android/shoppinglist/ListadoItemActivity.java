package com.fjnavarro.android.shoppinglist;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class ListadoItemActivity extends ListActivity {

	int list_id;
	String nombreLista;
	AdaptadorItem adaptador;
	private SQLiteDatabase db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listado_item);
		
		try{
			// Nos conectamos a la base de datos
			BDDAssistan BDDAss = new BDDAssistan(this);
			db = BDDAss.getWritableDatabase();
			
			//Recuperamos la informaci�n pasada en el intent
	        Bundle bundle = this.getIntent().getExtras();
	        
			this.list_id = bundle.getInt("id_lista");
			this.nombreLista = bundle.getString("nombreLista");
			
			// modificamos el t�tulo de la vista para que tenga el mismo nombre que la lista
			setTitle(nombreLista);
			
			// Cargamos las listas que tengamos en la bd.
			String[] args = new String[] {String.valueOf(this.list_id)};
			Cursor c = db.rawQuery("SELECT id, list_id, name, buyed FROM element WHERE list_id=?",args);

			// creamos un lista de array con clases "Item" 
			ArrayList<Item> datos = new ArrayList<Item>();

			if (c.moveToFirst()) {
				//Recorremos el cursor hasta que no haya m�s registros
				do {
					//vamos a�adiendo nuevos "Item" al array
					datos.add(new Item(c.getInt(0),c.getInt(1),c.getString(2),c.getInt(3)));
				} while(c.moveToNext());
			}			
			
			// convertimos y enviamos el array al layout
			adaptador = new AdaptadorItem(this, datos);

			setListAdapter(adaptador);
			
			//Asociamos los men�s contextuales a los controles	    
		    registerForContextMenu(getListView());
		}catch(SQLiteException e){
			// Capturamos un error grave con la base de datos
			Log.e("Shopping List", "Error grave con la BD "+e.toString());
		}catch(Exception e){
			// Capturamos un error grave y no es de la bd
			Log.e("Shopping List", "Error grave "+e.toString());
		}finally{
			// liberamos recursos
			// cerramos la conexi�n con la bd
			db.close();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_listado_item, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		// Damos funcionalidad al action bar
		Intent intent = new Intent();
		
		switch (item.getItemId()) {
			case android.R.id.home:
				// vamos a la activity inicial
				intent.setClass(this, MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				return true;
			case R.id.menu_new:
				// vamos al activity crear nuevo item
				intent.setClass(ListadoItemActivity.this, AltaItemActivity.class);	
				Bundle b = new Bundle();
				b.putInt("id_lista", this.list_id);
				b.putString("nombreLista", this.nombreLista);
				intent.putExtras(b);	
				startActivity(intent);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		// mostramos un men� contextual al mantener presi�n sobre un item
		
		MenuInflater inflater = getMenuInflater();

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		
		// como t�tulo del menu contextual ponemos el nombre del item presionado
		menu.setHeaderTitle(this.adaptador.getItem(info.position).getNombre());

		// definimos el menu a mostrar 
		inflater.inflate(R.menu.menu_ctx_listado, menu);

	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		// creamos las acciones al pulsar en una opci�n del men� contextual

	    switch (item.getItemId()) {
	        case R.id.CtxLstOpc1:
	        	try{
		        	// Nos conectamos a la base de datos para eliminar la lista
		    		BDDAssistan BDDAss = new BDDAssistan(this);
		    		db = BDDAss.getWritableDatabase();

	    			db.execSQL("DELETE FROM element WHERE id="
	    					+ this.adaptador.getItem(info.position).getId());
	    			// tambi�n lo borramos en el adaptador
	    			this.adaptador.deleteItem(info.position);
	    			// notificamos a la vista que hay que actualizar los datos.
	    			this.adaptador.notifyDataSetChanged();
	    		}catch(SQLiteException e){
	    			// Capturamos un error grave con la base de datos
	    			Log.e("Shopping List", "Error grave con la BD "+e.toString());
	    		}catch(Exception e){
	    			// Capturamos un error grave y no es de la bd
	    			Log.e("Shopping List", "Error grave "+e.toString());
	    		}finally{
	    			// liberamos recursos
	    			// cerarmos la conexi�n con la bd
	    			db.close();
	    		}
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
}
