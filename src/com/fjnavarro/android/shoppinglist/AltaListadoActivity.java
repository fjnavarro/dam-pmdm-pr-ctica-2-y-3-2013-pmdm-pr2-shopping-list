package com.fjnavarro.android.shoppinglist;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AltaListadoActivity extends Activity {

	private BDDAssistan BDDAss;
	private SQLiteDatabase db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alta_listado);
		
		// obtenemos una referencia a los controles del interface
		final EditText txtNombre = (EditText)findViewById(R.id.TxtNombre);
		final Button btnGuardar = (Button)findViewById(R.id.BtnGuardar);
		final Button btnCancelar = (Button)findViewById(R.id.BtnCancelar);
		
		try{
			// Nos conectamos a la base de datos
			BDDAss = new BDDAssistan(this);
		}catch(SQLiteException e){
			// Capturamos un error grave con la base de datos
			Log.e("Shopping List", "Error grave con la BD "+e.toString());
		}catch(Exception e){
			// Capturamos un error grave y no es de la bd
			Log.e("Shopping List", "Error grave "+e.toString());
		}
		
		// Implementamos el evento onclick del bot�n guardar
		btnGuardar.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				String nombre = txtNombre.getText().toString();
				
				if(!nombre.equals("")){
					try{
						// creamos una nueva lista en la bd
						db = BDDAss.getWritableDatabase();
					
						// Insertamos un registro en la bd.
						db.execSQL("INSERT INTO list (name) VALUES ('"+nombre+"')");	
					}catch(SQLiteException e){
		    			// Capturamos un error grave con la base de datos
		    			Log.e("Shopping List", "Error grave con la BD "+e.toString());
		    		}catch(Exception e){
		    			// Capturamos un error grave y no es de la bd
		    			Log.e("Shopping List", "Error grave "+e.toString());
		    		}finally{
		    			// liberamos recursos
		    			// cerarmos la conexi�n con la bd
		    			db.close();
		    		}
				}
				
				// volvemos al listado de listas.
				Intent intent = new Intent(AltaListadoActivity.this,MainActivity.class);
				// Iniciamos la nueva actividad
				startActivity(intent);
				// borramos esta actividad de la pila de activities
				finish();
			}
		});
		
		// Implementamos el evento onclick del bot�n cancelar
		btnCancelar.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){			
				Intent intent = new Intent(AltaListadoActivity.this,MainActivity.class);
				// Iniciamos la nueva actividad
				startActivity(intent);
				// borramos esta actividad de la pila de activities
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_alta_listado, menu);
		return true;
	}

}
