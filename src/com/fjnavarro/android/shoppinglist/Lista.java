package com.fjnavarro.android.shoppinglist;
/*
 * Almacena registros de la tabla list
 * */
public class Lista {
	private int id;
	private String nombre;
	
	public Lista(int id, String nombre){
		this.id = id;
		this.nombre = nombre;
	}
	public int getId(){
		return this.id;
	}
	public String getNombre(){
		return this.nombre;
	}
}
