package com.fjnavarro.android.shoppinglist;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.ListActivity;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class MainActivity extends ListActivity {

	AdaptadorLista adaptador;
	private SQLiteDatabase db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	    
		try{
			// Nos conectamos a la base de datos
			BDDAssistan BDDAss = new BDDAssistan(this);
			db = BDDAss.getWritableDatabase();

			// Cargamos las listas que tengamos en la bd.			
			Cursor c = db.rawQuery("SELECT id, name FROM list",null);
						
			// creamos un lista de array con clases "Lista" 		
			ArrayList<Lista> datos = new ArrayList<Lista>();
			
			if (c.moveToFirst()) {
			     //Recorremos el cursor hasta que no haya m�s registros en la bd
			     do {
			    	 //vamos a�adiendo nuevas "Lista" al array
			    	 datos.add(new Lista(c.getInt(0),c.getString(1)));
			     } while(c.moveToNext());
			}
			
			// convertimos y enviamos el array al layout
			adaptador = new AdaptadorLista(this, datos);
			 			 
			setListAdapter(adaptador);
			
		    //Asociamos los men�s contextuales a los controles	    
		    registerForContextMenu(getListView());			
		}catch(SQLiteException e){
			// Capturamos un error grave con la base de datos
			Log.e("Shopping List", "Error grave con la BD "+e.toString());
		}catch(Exception e){
			// Capturamos un error grave y no es de la bd
			Log.e("Shopping List", "Error grave "+e.toString());
		}finally{
			// liberamos recursos
			// cerarmos la conexi�n con la bd
			db.close();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		// Damos funcionalidad al action bar
		Intent intent = new Intent();
		
		if(item.getItemId()==R.id.menu_new){
			// si pulsamos en el bot�n nueva lista vamos al activity del alta de listas
			intent.setClass(MainActivity.this, AltaListadoActivity.class);
			startActivity(intent);
            return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // Damos funcionalidad a la lista de listas
        
        // al pulsar en una lista vamos a un listado de items
        Intent intent = new Intent(MainActivity.this,ListadoItemActivity.class);
        
        Bundle b = new Bundle();
        // a la nueva vista env�amos el id de la lista pulsada y su nombre
        b.putInt("id_lista",this.adaptador.getItem(position).getId());
        b.putString("nombreLista",this.adaptador.getItem(position).getNombre());
        intent.putExtras(b);
        
        startActivity(intent);
    }
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		// al tener unos segundos presionados una Lista de la lista de elementos se despliega un men� contextual
		MenuInflater inflater = getMenuInflater();

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

		// en el men� contextual ponemos el nombre de la lista presionada (t�tulo en la popup)
		menu.setHeaderTitle(this.adaptador.getItem(info.position).getNombre());
		
		// cargamos el layout del menu contextual
		inflater.inflate(R.menu.menu_ctx_listado, menu);

	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		// creamos las acciones al pulsar en una opci�n del men� contextual
		
	    switch (item.getItemId()) {
	        case R.id.CtxLstOpc1:
	        	try{
	        		// Nos conectamos a la base de datos para eliminar la lista
		    		BDDAssistan BDDAss = new BDDAssistan(this);
		    		db = BDDAss.getWritableDatabase();	    		
	
					db.execSQL("DELETE FROM list WHERE id="
							+ this.adaptador.getItem(info.position).getId());
					// tambi�n lo borramos en el adaptador
					this.adaptador.deleteItem(info.position);
					// notificamos a la vista que hay que actualizar los datos.
					this.adaptador.notifyDataSetChanged();
	        	}catch(SQLiteException e){
	    			// Capturamos un error grave con la base de datos
	    			Log.e("Shopping List", "Error grave con la BD "+e.toString());
	    		}catch(Exception e){
	    			// Capturamos un error grave y no es de la bd
	    			Log.e("Shopping List", "Error grave "+e.toString());
	    		}finally{
	    			// liberamos recursos
	    			// cerarmos la conexi�n con la bd
	    			db.close();
	    		}
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
}
