package com.fjnavarro.android.shoppinglist;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;

public class BDDAssistan extends SQLiteOpenHelper {
	
	// versi�n de la base de datos
	private static final int BDD_VERSION=1;
	// nombre de la base de datos
	private static final String BDD_NAME="shoppingList";

	public BDDAssistan(Context context){
		super(context, BDD_NAME, null, BDD_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// creamos las tablas
		arg0.execSQL("CREATE TABLE list (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name STRING NOT NULL);");
		arg0.execSQL("CREATE TABLE element (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				" list_id INTEGER NOT NULL CONSTRAINT fk_list_id REFERENCES a(id) ON DELETE CASCADE," +
				" name STRING NOT NULL, buyed BOOL NOT NULL);");
		// creamos los triggers para actualizar las referencias
		// insert
		arg0.execSQL("CREATE TRIGGER fki_element_list_id " +
				" BEFORE INSERT ON element " +
				" FOR EACH ROW BEGIN " +
				" SELECT CASE " +
				" WHEN ((SELECT id FROM list WHERE id = NEW.list_id) IS NULL) " +
				" THEN RAISE(ABORT, 'Foreign Key Violation ') " +				
				" END; " +
				" END; ");
		// update
		arg0.execSQL("CREATE TRIGGER fku_element_list_id " +
				" BEFORE UPDATE ON element " +
				" FOR EACH ROW BEGIN " +
				" SELECT CASE " +
				" WHEN ((SELECT id FROM list WHERE id = NEW.list_id) IS NULL) " +
				" THEN RAISE(ABORT, 'update on table Foreign Key Violation ') " +				
				" END; " +
				" END; ");
		// update - al eliminar una list elimina los element relacionados
		arg0.execSQL("CREATE TRIGGER fkd_element_list_id "
				+ " BEFORE DELETE ON list "
				+ " FOR EACH ROW BEGIN "
				+ " DELETE from element WHERE list_id = OLD.id; "
				+ " END; ");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
