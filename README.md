# Aplicación Android pmdm-pr2-shopping-list - Aplicación de gestión de listas de la compra #

*Comparto el siguiente código realizado en el Técnico Superior de Desarrollo de Aplicaciones Multiplataforma.

*Se realizó como una práctica de la asignatura de “Programación Multimedia y Dispositivos Móviles”, es una aplicación Android.

*Hay que tener en cuenta que sólo se pudo utilizar las tecnologías y técnicas dadas en el trimestre de la práctica.

*La práctica se realizó el 05-05-2013

*La práctica son dos, la segunda y la tercera, que era una ampliación de la segunda.

===================


## Texto de la práctica ##

PR2 - Aplicación de gestión de listas de la compra

Durante el segundo trimestre hemos visto como utilizar la interface de usuario y como guardar datos.

Realizaremos una aplicación que permita gestionar varias listas de la compra:

• Crear una lista

• Añadir elementos a una lista data

• Marcar los como comprado

• Borrar elementos


PR3 - Aplicación de gestión visual de listas de la compra

Durante el tercer trimestre hemos visto cómo utilizar la interface de usuario y cómo guardar datos y como visualizar contenidos gráficos.

Completaremos la aplicación que permite gestionar varias listas de la compra, permitiendo asociar recursos gráficos y multimedia a los distintos productos:

• Los elementos de la lista pueden tener asociados imágenes u otros contenidos multimedia que podemos tener en el teléfono o accesibles por internet.

• Al marcarlos como comprados el componente visual indicará de forma clara que y no están pendientes.

## Nota ##

*Esta aplicación requiere Android 3.0 o superior.

￼Todas las imágenes se almacenan en el directorio de imágenes de la memoria externa de la aplicación, en el caso de esta aplicación suele ￼￼estar ubicado en el SD /Android/data/com.fjnavarro.android.shoppinglist/files/Pictures/

## Screenshots ##

Aplicación funcionando

[![Aplicación funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr2-shopping-list/capturaPantallaCodigo.png)](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr2-shopping-list/capturaPantallaCodigo.png)

Diseño funcional

[![Diseño funcional](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr2-shopping-list/disenioFuncional.png)](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr2-shopping-list/disenioFuncional.png)

Modelo de datos

[![Modelo de datos](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr2-shopping-list/ModeloDeDatos.png)](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr2-shopping-list/ModeloDeDatos.png)

## Autor ##

* Francisco José Navarro García <fran@fjnavarro.com>
* Twitter : *[@fjnavarro_](https://twitter.com/fjnavarro_)*
* Linkedin: *[https://www.linkedin.com/in/fjnavarrogarcia](https://www.linkedin.com/in/fjnavarrogarcia)*
* Blog    : *[http://www.fjnavarro.com/](http://www.fjnavarro.com/)*